<?php

// routes that start with url "billing/"
$router->group(['prefix' => 'billing'], function () use ($router) {
	// create new service
    $router->post('/service/new', 'service\ServiceController@newService');

    // add new service to user
    $router->post('/service/add-to-user', 'service\ServiceController@addServiceToUser');

    // add new appointment to user
    $router->post('/appointment/add-to-user', 'userAppointment\UserAppointmentController@addAppointmentToUser');

    // create new form
    $router->post('/form/new', 'form\FormController@newForm');

    // add form fields
    $router->post('/form/add-field', 'form\FormController@addFormField');

    // create new form answers for a user
    $router->post('/form/new-user-form', 'form\FormController@newUserForm');
});