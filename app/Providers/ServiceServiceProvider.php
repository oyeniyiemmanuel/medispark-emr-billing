<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ServiceRepository;
use App\Repositories\UserVisitRepository;
use App\Repositories\UserAppointmentRepository;
use App\Repositories\Interfaces\ServiceInterface;
use App\Repositories\Interfaces\UserVisitInterface;
use App\Repositories\Interfaces\UserAppointmentInterface;

class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ServiceInterface::class, 
            ServiceRepository::class
        );

        $this->app->bind(
            UserVisitInterface::class, 
            UserVisitRepository::class
        );

        $this->app->bind(
            UserAppointmentInterface::class, 
            UserAppointmentRepository::class
        );
    }
}
