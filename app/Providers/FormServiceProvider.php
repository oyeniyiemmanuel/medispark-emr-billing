<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\FormRepository;
use App\Repositories\Interfaces\FormInterface;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            FormInterface::class, 
            FormRepository::class
        );
    }
}
