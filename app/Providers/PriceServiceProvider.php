<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\PriceRepository;
use App\Repositories\Interfaces\PriceInterface;

class PriceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PriceInterface::class, 
            PriceRepository::class
        );
    }
}
