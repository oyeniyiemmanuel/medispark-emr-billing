<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserVisitInterface;
use App\Repositories\Interfaces\PriceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\UserVisit;

class UserVisitRepository implements UserVisitInterface
{
	/**
     * store new UserVisit
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\UserVisit
     */
    public function newUserVisit(Request $request)
    {
        // store new UserVisit details
        $new_userVisit = UserVisit::create([
                        "user_id" => $request->user_id,
                        "organization_id" => $request->organization_id,
                        "branch_id" => $request->branch_id,
                        "retainer_id" => $request->retainer_id,
                        "type" => $request->visit_type,
                    ]);

        return $new_userVisit;
    }

    /**
     * Get UserVisit using ID
     * @param  int $userVisit_id
     * @return array
     */
    public function getUserVisitById($userVisit_id)
    {
        return UserVisit::findOrFail($userVisit_id);
    }
}