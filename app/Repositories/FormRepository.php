<?php

namespace App\Repositories;

use App\Repositories\Interfaces\FormInterface;
use App\Repositories\Interfaces\ServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\UserForm;
use App\Models\Form;
use App\Models\FormNumberQuestion;
use App\Models\FormTextQuestion;
use App\Models\FormRadioQuestion;
use App\Models\FormSelectQuestion;
use App\Models\FormMultiSelectQuestion;
use App\Models\FormNumberAnswer;
use App\Models\FormTextAnswer;
use App\Models\FormRadioAnswer;
use App\Models\FormSelectAnswer;
use App\Models\FormMultiSelectAnswer;
use App\Models\UserVisit;

class FormRepository implements FormInterface
{
    /**
     * private declaration of repositories
     *
     * @var priceRepository
     */
    private $serviceRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $priceRepository
     * @return void
     */
    public function __construct(ServiceInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

	/**
     * store new Form
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\Form
     */
    public function newForm(Request $request)
    {
        // store new Form details
        $new_form = Form::firstOrCreate([
                        "name" => $request->name,
                        "organization_id" => $request->organization_id,
                        "branch_id" => $request->branch_id
                    ]);

        // if services isset in the request
        if ($request->filled('services')) {
            // attach the new form to each of the service
            foreach ($request->services as $service_id) {

                // attach newly created form to the service
                $new_form->services()->syncWithoutDetaching([
                                                        $service_id => [
                                                                'organization_id' => $request->organization_id, 
                                                                'branch_id' => $request->branch_id
                                                            ]
                                                        ]);  
            }
        }

        return $new_form;
    }

    /**
     * add form fields
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\Form
     */
    public function addFormField(Request $request)
    { 
        // get form instance
        $form = $this->getFormById($request->form_id);

        // if form_fields isset in the request
        if ($request->filled('form_fields')) {

            // add new form number question
           $this->newFormNumberQuestion($request, $form);

            // add new form text question
           $this->newFormTextQuestion($request, $form);

            // add new form radio question
           $this->newFormRadioQuestion($request, $form);

            // add new form select question
           $this->newFormSelectQuestion($request, $form);

            // add new form multiselect question
           $this->newFormMultiSelectQuestion($request, $form);
        }

        return $form;
    }

    /**
     * store new form ansers for a user
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\UserForm
     */
    public function newUserForm(Request $request)
    { 
        // get form instance
        $form = $this->getFormById($request->form_id);

        $new_user_form = [];

        // if form_fields isset in the request
        if ($request->filled('form_fields')) {

            // create new userForm model
            $new_user_form = UserForm::create(
                                            [
                                                'user_id' => $request->user_id,
                                                'organization_id' => $request->organization_id,
                                                'branch_id' => $request->branch_id,
                                            ]
                                        );
            // attach the newly create user_form to a form
            $form->userForms()->save($new_user_form);

            // store new number answer
            $this->newFormNumberAnswer($request, $new_user_form);

            // store new text answer
            $this->newFormTextAnswer($request, $new_user_form);

            // store new radio answer
            $this->newFormRadioAnswer($request, $new_user_form);

            // store new select answer
            $this->newFormSelectAnswer($request, $new_user_form);

            // store new multiselect answer
            $this->newFormMultiSelectAnswer($request, $new_user_form);
            
        }

        return $new_user_form;
    }

    /**
     * Store new Number answer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\UserForm  $user_form
     * @return App\Models\FormNumberAnswer
     */
    public function newFormNumberAnswer(Request $request, UserForm $user_form)
    {
        $form_number_answer = [];

        // if there are Number fields and number_answers is filled
        if (in_array('number', $request->form_fields) && $request->filled('number_answers')) {
            foreach ($request->number_answers as $answer) {
                // create new form_number_answer
                $form_number_answer = FormNumberAnswer::firstOrCreate(
                                        [
                                            'user_id' => $request->user_id,
                                            'user_form_id' => $user_form->id,
                                            'form_number_question_id' => $answer['form_number_question_id'],
                                            'value' => $answer['value'],
                                        ]
                                    );
            }
        }

        return $form_number_answer;
    }

    /**
     * Store new text answer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\UserForm  $user_form
     * @return App\Models\FormTextAnswer
     */
    public function newFormTextAnswer(Request $request, UserForm $user_form)
    {
        $form_text_answer = [];

        // if there are text fields and text_answers is filled
        if (in_array('text', $request->form_fields) && $request->filled('text_answers')) {
            foreach ($request->text_answers as $answer) {
                // create new form_text_answer
                $form_text_answer = FormTextAnswer::firstOrCreate(
                                        [
                                            'user_id' => $request->user_id,
                                            'user_form_id' => $user_form->id,
                                            'form_text_question_id' => $answer['form_text_question_id'],
                                            'value' => $answer['value'],
                                        ]
                                    );
            }
        }

        return $form_text_answer;
    }

    /**
     * Store new radio answer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\UserForm  $user_form
     * @return App\Models\FormRadioAnswer
     */
    public function newFormRadioAnswer(Request $request, UserForm $user_form)
    {
        $form_radio_answer = [];

        // if there are radio fields and radio_answers is filled
        if (in_array('radio', $request->form_fields) && $request->filled('radio_answers')) {
            foreach ($request->radio_answers as $answer) {
                // create new form_radio_answer
                $form_radio_answer = FormRadioAnswer::firstOrCreate(
                                        [
                                            'user_id' => $request->user_id,
                                            'user_form_id' => $user_form->id,
                                            'form_radio_question_id' => $answer['form_radio_question_id'],
                                            'value' => $answer['value'],
                                        ]
                                    );
            }
        }

        return $form_radio_answer;
    }

    /**
     * Store new Select answer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\UserForm  $user_form
     * @return App\Models\FormSelectAnswer
     */
    public function newFormSelectAnswer(Request $request, UserForm $user_form)
    {
        $form_select_answer = [];

        // if there are Select fields and select_answers is filled
        if (in_array('select', $request->form_fields) && $request->filled('select_answers')) {
            foreach ($request->select_answers as $answer) {
                // create new form_select_answer
                $form_select_answer = FormSelectAnswer::firstOrCreate(
                                        [
                                            'user_id' => $request->user_id,
                                            'user_form_id' => $user_form->id,
                                            'form_select_question_id' => $answer['form_select_question_id'],
                                            'value' => $answer['value'],
                                        ]
                                    );
            }
        }

        return $form_select_answer;
    }

    /**
     * Store new MultiSelect answer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\UserForm  $user_form
     * @return App\Models\FormMultiSelectAnswer
     */
    public function newFormMultiSelectAnswer(Request $request, UserForm $user_form)
    {
        $form_multi_select_answer = [];

        // if there are MultiSelect fields and multi_select_answers is filled
        if (in_array('multi-select', $request->form_fields) && $request->filled('multi_select_answers')) {
            foreach ($request->multi_select_answers as $answer) {
                // create new form_multi_select_answer
                $form_multi_select_answer = FormMultiSelectAnswer::firstOrCreate(
                                        [
                                            'user_id' => $request->user_id,
                                            'user_form_id' => $user_form->id,
                                            'form_multi_select_question_id' => $answer['form_multi_select_question_id'],
                                            'value' => $answer['value'],
                                        ]
                                    );
            }
        }

        return $form_multi_select_answer;
    }

    /**
     * Store new Number Question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Form $form
     * @return App\Models\FormNumberQuestion
     */
    public function newFormNumberQuestion(Request $request, Form $form)
    {
        $form_number_question = [];

        // if there are Number fields and number_details is filled
        if (in_array('number', $request->form_fields) && $request->filled('number_details')) {
            foreach ($request->number_details as $option) {
                // create new form_number_question
                $form_number_question = FormNumberQuestion::firstOrCreate(
                                        [
                                            'form_id' => $form->id,
                                            'name' => $option['name'],
                                        ]
                                    );
            }
        }

        return $form_number_question;
    }

    /**
     * Store new text Question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Form $form
     * @return App\Models\FormTextQuestion
     */
    public function newFormTextQuestion(Request $request, Form $form)
    {
        $form_text_question = [];

        // if there are text fields and text_details is filled
        if (in_array('text', $request->form_fields) && $request->filled('text_details')) {
            foreach ($request->text_details as $option) {
                // create new form_text_question
                $form_text_question = FormTextQuestion::firstOrCreate(
                                        [
                                            'form_id' => $form->id,
                                            'name' => $option['name'],
                                        ]
                                    );
            }
        }

        return $form_text_question;
    }

    /**
     * Store new radio Question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Form $form
     * @return App\Models\FormRadioQuestion
     */
    public function newFormRadioQuestion(Request $request, Form $form)
    {
        $form_radio_question = [];

        // if there are radio fields and radio_details is filled
        if (in_array('radio', $request->form_fields) && $request->filled('radio_details')) {
            foreach ($request->radio_details as $option) {
                // create new form_radio_question
                $form_radio_question = FormRadioQuestion::firstOrCreate(
                                        [
                                            'form_id' => $form->id,
                                            'name' => $option['name'],
                                            'options' => $option['options'],
                                        ]
                                    );
            }
        }

        return $form_radio_question;
    }

    /**
     * Store new Select Question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Form $form
     * @return App\Models\FormSelectQuestion
     */
    public function newFormSelectQuestion(Request $request, Form $form)
    {
        $form_select_question = [];

        // if there are Select fields and select_details is filled
        if (in_array('select', $request->form_fields) && $request->filled('select_details')) {
            foreach ($request->select_details as $option) {
                // create new form_select_question
                $form_select_question = FormSelectQuestion::firstOrCreate(
                                        [
                                            'form_id' => $form->id,
                                            'name' => $option['name'],
                                            'options' => $option['options'],
                                        ]
                                    );
            }
        }

        return $form_select_question;
    }

    /**
     * Store new MultiSelect Question
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Form $form
     * @return App\Models\FormMultiSelectQuestion
     */
    public function newFormMultiSelectQuestion(Request $request, Form $form)
    {
        $form_multi_select_question = [];

        // if there are MultiSelect fields and multi_select_details is filled
        if (in_array('multi-select', $request->form_fields) && $request->filled('multi_select_details')) {
            foreach ($request->multi_select_details as $option) {
                // create new form_multi_select_question
                $form_multi_select_question = FormMultiSelectQuestion::firstOrCreate(
                                        [
                                            'form_id' => $form->id,
                                            'name' => $option['name'],
                                            'options' => $option['options'],
                                        ]
                                    );
            }
        }

        return $form_multi_select_question;
    }

    /**
     * Get Form using ID
     * @param  int $form_id
     * @return array
     */
    public function getFormById($form_id)
    {
        return Form::findOrFail($form_id);
    }

    /**
     * Get the Form of a user by the user_form_id
     * @param  int $form_id
     * @return array
     */
    public function getUserFormById($user_form_id)
    {
        return UserForm::findOrFail($user_form_id);
    }
}