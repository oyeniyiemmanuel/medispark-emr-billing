<?php

namespace App\Repositories;

use App\Repositories\Interfaces\ServiceInterface;
use App\Repositories\Interfaces\PriceInterface;
use App\Repositories\Interfaces\UserVisitInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ServiceUser;
use App\Models\Service;
use App\Models\UserVisit;

class ServiceRepository implements ServiceInterface
{
    /**
     * private declaration of repositories
     *
     * @var priceRepository
     */
    private $priceRepository;
    private $userVisitRepository;

    /**
     * Dependency Injection of some repositories.
     *
     * @param  \App\Repositories\Interfaces\UserInterface  $priceRepository
     * @return void
     */
    public function __construct(PriceInterface $priceRepository, UserVisitInterface $userVisitRepository)
    {
        $this->priceRepository = $priceRepository;
        $this->userVisitRepository = $userVisitRepository;
    }

	/**
     * store new Service
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\Service
     */
    public function newService(Request $request)
    {
        // store new Service details
        $new_service = Service::firstOrCreate([
                        "name" => $request->name,
                        "category" => $request->category,
                        "care_type" => $request->care_type,
                        "organization_id" => $request->organization_id,
                        "branch_id" => $request->branch_id
                    ]);

        // if fee_paying_retainers isset in the request
        if ($request->filled('fee_paying_retainers')) {
            // foreach fee_paying_retainer, create a new price model and add retainer_id to it
            foreach ($request->fee_paying_retainers as $retainer_id) {

                // add retainer_id and service_id attributes to the request array
                $request->request->add(["service_id" => $new_service->id]);
                $request->request->add(["retainer_id" => $retainer_id]);

                // store new Price details
                $price = $this->priceRepository->newPrice($request);
            }
        }

        $new_service->with("prices");

        return $new_service;
    }

    /**
     * add service to user
     * @param  \Illuminate\Http\Request  $request, See gateway.ServiceController.AddServiceToUser validation param
     * @return App\Models\Service
     */
    public function addServiceToUser(Request $request)
    {
        // if services isset in the request
        if ($request->filled('services')) {
            // foreach service, create a new price model and add retainer_id to it
            foreach ($request->services as $service_id) {

                // find the service by id
                $service = $this->getServiceById($service_id);

                // add service to user on service_user table
                $service_user = ServiceUser::create(
                                            [
                                                'service_id' => $service->id,
                                                'user_id' => $request->user_id,
                                                'organization_id' => $request->organization_id,
                                                'branch_id' => $request->branch_id,
                                                'retainer_category' => $request->retainer_category,
                                                'service_state' => $request->service_state,
                                            ]
                                        );

                // ADD SERVICE_STATE BASED ON THE SERVICE CATEGORY
                if ($service->category == 'registration' || $service->category == 'consultation') {
                    $service_user->service_state = 'confirmed';
                    $service_user->save();
                } else {
                    $service_user->service_state = 'triggered';
                    $service_user->save();
                }

                // PERFORM VISIT RELATED ACTIONS BASED ON visit_status:
                // if this is a new visit, create it and add service to it
                if ($request->visit_status == 'new' && $request->filled('visit_type')) {

                    $new_visit = $this->userVisitRepository->newUserVisit($request);

                    // add service to user's visit
                    $this->addServiceUserToUserVisit($service_user->id, $new_visit->id);

                } 
                // if visit is old, only add service to it
                if ($request->visit_status == 'old' && $request->filled('visit_id')) {

                    // add service to user's visit
                    $this->addServiceUserToUserVisit($service_user->id, $request->visit_id);

                }
            }
        }

        $service_user->refresh();

        return $service_user;
    }

    /**
     * attach service to a user's visit
     * @param  int  $service_id
     * @param  int  $visit_id
     * @return void
     */
    public function addServiceUserToUserVisit($service_id, $visit_id)
    {
        // get user_visit
        $user_visit = $this->userVisitRepository->getUserVisitById($visit_id);

        // get user
        $service_user = $this->getServiceUserById($service_id);

        // add service to user's visit
        $user_visit->Services()->save($service_user);   
    }

    /**
     * Get service using ID
     * @param  int $service_id
     * @return array
     */
    public function getServiceById($service_id)
    {
        return Service::findOrFail($service_id);
    }

    /**
     * Get the service of a user by the service_user_id
     * @param  int $service_id
     * @return array
     */
    public function getServiceUserById($service_user_id)
    {
        return ServiceUser::findOrFail($service_user_id);
    }
}