<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PriceInterface;
use Illuminate\Http\Request;
use App\Models\Retainer;
use App\Models\Service;
use App\Models\Price;

class PriceRepository implements PriceInterface
{
	/**
     * store new Price
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\Price
     */
    public function newPrice(Request $request)
    {
        // store new Price details
        $new_price = Price::firstOrCreate([
                        "amount" => $request->amount,
                        "service_id" => $request->service_id,
                        "retainer_id" => $request->retainer_id,
                    ]);

        return $new_price;
    }

    /**
     * attach price to a service
     * @param  int  $price_id
     * @param  int  $service_id
     * @return void
     */
    public function addPriceToService($price_id, $service_id)
    {
        // get service
        $service = Service::findOrFail($service_id);

        // get price
        $price = $this->getPriceById($price_id);

        // add price to service
        $service->prices()->save($price); 
    }

    /**
     * attach price to a retainer
     * @param  int  $price_id
     * @param  int  $retainer_id
     * @return void
     */
    public function addPriceToRetainer($price_id, $retainer_id)
    {
        // // get retainer
        // $retainer = Retainer::findOrFail($retainer_id);

        // // get price
        // $price = $this->getPriceById($price_id);

        // // sync price to the retainer without removing previous users, also don't sync if its done before
        // $retainer->prices()->save($price); 
    }

     /**
     * Get price using ID
     * @param  int $price_id
     * @return App\Models\Price
     */
    public function getPriceById($price_id)
    {
        return Price::findOrFail($price_id);
    }
}