<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Price;

interface PriceInterface
{
    public function newPrice(Request $request);

    public function addPriceToService($price_id, $service_id);

    public function addPriceToRetainer($price_id, $retainer_id);
}