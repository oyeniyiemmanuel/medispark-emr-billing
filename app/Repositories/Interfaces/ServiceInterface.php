<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\Service;

interface ServiceInterface
{
    public function newService(Request $request);

    public function addServiceToUser(Request $request);

    public function addServiceUserToUserVisit($service_id, $visit_id);

    public function getServiceById($service_id);

    public function getServiceUserById($service_user_id);
}