<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\UserForm;
use App\Models\Form;

interface FormInterface
{
    public function newForm(Request $request);

    public function addFormField(Request $request);

    public function newUserForm(Request $request);

    public function newFormRadioAnswer(Request $request, UserForm $user_form);
    
    public function newFormTextAnswer(Request $request, UserForm $user_form);

    public function newFormSelectAnswer(Request $request, UserForm $user_form);

    public function newFormTextQuestion(Request $request, Form $form);

    public function newFormRadioQuestion(Request $request, Form $form);

    public function newFormSelectQuestion(Request $request, Form $form);

    public function newFormMultiSelectQuestion(Request $request, Form $form);

    public function getFormById($form_id);

    public function getUserFormById($user_form_id);
}