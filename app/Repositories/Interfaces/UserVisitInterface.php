<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\UserVisit;

interface UserVisitInterface
{
    public function newUserVisit(Request $request);

    public function getUserVisitById(Request $request);
}