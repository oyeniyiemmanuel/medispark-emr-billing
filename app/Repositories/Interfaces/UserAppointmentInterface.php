<?php

namespace App\Repositories\Interfaces;
use Illuminate\Http\Request;
use App\Models\UserAppointment;

interface UserAppointmentInterface
{
    public function addAppointmentToUser(Request $request);

    public function getUserAppointmentById(Request $request);
}