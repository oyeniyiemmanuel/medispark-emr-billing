<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserAppointmentInterface;
use App\Repositories\Interfaces\PriceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\UserAppointment;

class UserAppointmentRepository implements UserAppointmentInterface
{
	/**
     * store new UserAppointment
     * @param  \Illuminate\Http\Request  $request
     * @return App\Models\UserAppointment
     */
    public function addAppointmentToUser(Request $request)
    {
        // if services isset in the request
        if ($request->filled('services')) {
            // foreach service, create a new user appointment
            foreach ($request->services as $service_id) {
                // store new UserAppointment details
                $new_userAppointment = UserAppointment::create([
                                "user_id" => $request->user_id,
                                "appointee_id" => $request->appointee_id,
                                "organization_id" => $request->organization_id,
                                "branch_id" => $request->branch_id,
                                "retainer_id" => $request->retainer_id,
                                "service_id" => $service_id,
                                "status" => "open",
                            ]);
            }
        }

        return $new_userAppointment;
    }

    /**
     * Get UserAppointment using ID
     * @param  int $userAppointment_id
     * @return array
     */
    public function getUserAppointmentById($userAppointment_id)
    {
        return UserAppointment::findOrFail($userAppointment_id);
    }
}