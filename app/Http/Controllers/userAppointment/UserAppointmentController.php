<?php

namespace App\Http\Controllers\userAppointment;

use App\Repositories\Interfaces\UserAppointmentInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;

class UserAppointmentController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Userappointment repository
     *
     * @var userAppointmentRepository
     */
    private $userAppointmentRepository;

    /**
     * Dependency Injection of userAppointmentRepository.
     *
     * @param  \App\Repositories\Interfaces\UserAppointmentInterface  $userAppointmentRepository
     * @return void
     */
    public function __construct(UserAppointmentInterface $userAppointmentRepository)
    {
        $this->userAppointmentRepository = $userAppointmentRepository;
    }

    /**
     * add appointment to user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addAppointmentToUser(Request $request)
    {
        return $this->successResponse($this->userAppointmentRepository->addAppointmentToUser($request));
    }
}