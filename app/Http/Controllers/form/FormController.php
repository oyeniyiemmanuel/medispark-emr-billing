<?php

namespace App\Http\Controllers\form;

use App\Repositories\Interfaces\FormInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;

class FormController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Form repository
     *
     * @var formRepository
     */
    private $formRepository;

    /**
     * Dependency Injection of formRepository.
     *
     * @param  \App\Repositories\Interfaces\FormInterface  $formRepository
     * @return void
     */
    public function __construct(FormInterface $formRepository)
    {
        $this->formRepository = $formRepository;
    }

    /**
     * store new Form
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newForm(Request $request)
    {
        return $this->successResponse($this->formRepository->newForm($request));
    }

    /**
     * store new Form ansers for a user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newUserForm(Request $request)
    {
        return $this->successResponse($this->formRepository->newUserForm($request));
    }

    /**
     * add form fields
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addFormField(Request $request)
    {
        return $this->successResponse($this->formRepository->addFormField($request));
    }

    /**
     * add Form to user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addFormToUser(Request $request)
    {
        return $this->successResponse($this->formRepository->addFormToUser($request));
    }
}