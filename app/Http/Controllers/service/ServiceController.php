<?php

namespace App\Http\Controllers\service;

use App\Repositories\Interfaces\ServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;

class ServiceController extends Controller
{
    /**
     * telling the class to inherit ApiResponse Trait
     */
    use ApiResponse;

    /**
     * declaration of Service repository
     *
     * @var serviceRepository
     */
    private $serviceRepository;

    /**
     * Dependency Injection of serviceRepository.
     *
     * @param  \App\Repositories\Interfaces\ServiceInterface  $serviceRepository
     * @return void
     */
    public function __construct(ServiceInterface $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * store new Service
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function newService(Request $request)
    {
        return $this->successResponse($this->serviceRepository->newService($request));
    }

    /**
     * add service to user
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Traits\ApiResponse
     */
    public function addServiceToUser(Request $request)
    {
        return $this->successResponse($this->serviceRepository->addServiceToUser($request));
    }
}