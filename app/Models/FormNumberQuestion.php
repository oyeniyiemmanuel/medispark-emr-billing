<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormNumberQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the form model for a formNumberQuestion.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formNumberAnswers of a formNumberQuestion.
     */
    public function numberAnswers()
    {
        return $this->hasMany('App\Models\FormNumberAnswer');
    }
}