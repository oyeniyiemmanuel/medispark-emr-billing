<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormNumberAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UserForm model for a FormNumberAnswer.
     */
    public function userForm()
    {
        return $this->belongsTo('App\Models\UserForm');
    }

    /**
     * Get the NumberQuestion model for a FormNumberAnswer.
     */
    public function numberQuestion()
    {
        return $this->belongsTo('App\Models\FormNumberQuestion', 'form_number_question_id');
    }
}