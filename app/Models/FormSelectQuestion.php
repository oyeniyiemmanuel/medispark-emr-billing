<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormSelectQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the form model for a formSelectQuestion.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formSelectAnswers of a formSelectQuestion.
     */
    public function selectAnswers()
    {
        return $this->hasMany('App\Models\FormSelectAnswer');
    }
}
