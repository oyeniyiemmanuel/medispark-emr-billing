<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'service_user';

    /**
     * Get the visit of a particular service that was rendered for a user.
     */
    public function visit()
    {
        return $this->belongsTo('App\Models\UserVisit');
    }
}