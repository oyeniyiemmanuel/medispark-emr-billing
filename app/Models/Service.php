<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the prices of a service.
     */
    public function prices()
    {
        return $this->hasMany('App\Models\Price');
    }

    /**
     * Get all the forms of a service.
     */
    public function forms()
    {
        return $this->belongsToMany('App\Models\Form');
    }

}
