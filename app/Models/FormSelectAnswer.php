<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormSelectAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UserForm model for a FormSelectAnswer.
     */
    public function userForm()
    {
        return $this->belongsTo('App\Models\UserForm');
    }

    /**
     * Get the SelectQuestion model for a FormSelectAnswer.
     */
    public function selectQuestion()
    {
        return $this->belongsTo('App\Models\FormSelectQuestion', 'form_select_question_id');
    }
}