<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormTextQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the form model for a formTextQuestion.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formTextAnswers of a formTextQuestion.
     */
    public function textAnswers()
    {
        return $this->hasMany('App\Models\FormTextAnswer');
    }
}