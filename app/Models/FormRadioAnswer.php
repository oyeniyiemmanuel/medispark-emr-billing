<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormRadioAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UserForm model for a FormRadioAnswer.
     */
    public function userForm()
    {
        return $this->belongsTo('App\Models\UserForm');
    }

    /**
     * Get the radioQuestion model for a FormRadioAnswer.
     */
    public function radioQuestion()
    {
        return $this->belongsTo('App\Models\FormRadioQuestion', 'form_radio_question_id');
    }
}