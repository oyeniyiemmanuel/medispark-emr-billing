<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormMultiSelectQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the form model for a formMultiSelectQuestion.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formMultiSelectAnswers of a formMultiSelectQuestion.
     */
    public function multiSelectAnswers()
    {
        return $this->hasMany('App\Models\FormMultiSelectAnswer');
    }
}