<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormRadioQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the form model for a formRadioQuestion.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formRadioAnswers of a formRadioQuestion.
     */
    public function radioAnswers()
    {
        return $this->hasMany('App\Models\FormRadioAnswer');
    }
}
