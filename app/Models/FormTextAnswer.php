<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormTextAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UserForm model for a FormTextAnswer.
     */
    public function userForm()
    {
        return $this->belongsTo('App\Models\UserForm');
    }

    /**
     * Get the textQuestion model for a FormTextAnswer.
     */
    public function textQuestion()
    {
        return $this->belongsTo('App\Models\FormTextQuestion', 'form_text_question_id');
    }
}