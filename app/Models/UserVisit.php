<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVisit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'user_visit';

    /**
     * Get the Services that were rendered to a user during a particular visit.
     */
    public function services()
    {
        return $this->hasMany('App\Models\ServiceUser');
    }

    /**
     * Get the appointment of a particular visit.
     */
    public function appointment()
    {
        return $this->belongsTo('App\Models\UserAppointment');
    }
}
