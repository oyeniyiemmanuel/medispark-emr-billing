<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the formNumberQuestions of a Form.
     */
    public function numberQuestions()
    {
        return $this->hasMany('App\Models\FormNumberQuestion');
    }

    /**
     * Get the formTextQuestions of a Form.
     */
    public function textQuestions()
    {
        return $this->hasMany('App\Models\FormTextQuestion');
    }

    /**
     * Get the formRadioQuestions of a Form.
     */
    public function radioQuestions()
    {
        return $this->hasMany('App\Models\FormRadioQuestion');
    }

    /**
     * Get the formSelectQuestions of a Form.
     */
    public function selectQuestions()
    {
        return $this->hasMany('App\Models\FormSelectQuestion');
    }

    /**
     * Get the formMultiSelectQuestions of a Form.
     */
    public function multiSelectQuestions()
    {
        return $this->hasMany('App\Models\FormMultiSelectQuestion');
    }

    /**
     * Get all the services of a form.
     */
    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }

    /**
     * Get all the userForms of a form.
     */
    public function userForms()
    {
        return $this->hasMany('App\Models\UserForm');
    }
}
