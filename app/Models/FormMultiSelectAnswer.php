<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormMultiSelectAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UserForm model for a FormMultiSelectAnswer.
     */
    public function userForm()
    {
        return $this->belongsTo('App\Models\UserForm');
    }

    /**
     * Get the MultiSelectQuestion model for a FormMultiSelectAnswer.
     */
    public function multiSelectQuestion()
    {
        return $this->belongsTo('App\Models\FormMultiSelectQuestion', 'form_multi_select_question_id');
    }
}