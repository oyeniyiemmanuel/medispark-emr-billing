<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the service a price belongs to.
     */
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
