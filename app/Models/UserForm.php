<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserForm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['result'];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'user_form';

    /**
     * Get the form that is related to the UserForm model.
     */
    public function form()
    {
        return $this->belongsTo('App\Models\Form');
    }

    /**
     * Get the formNumberAnswers of a Form.
     */
    public function numberAnswers()
    {
        return $this->hasMany('App\Models\FormNumberAnswer');
    }

    /**
     * Get the formTextAnswers of a Form.
     */
    public function textAnswers()
    {
        return $this->hasMany('App\Models\FormTextAnswer');
    }

    /**
     * Get the formRadioAnswers of a Form.
     */
    public function radioAnswers()
    {
        return $this->hasMany('App\Models\FormRadioAnswer');
    }

    /**
     * Get the formSelectAnswers of a Form.
     */
    public function selectAnswers()
    {
        return $this->hasMany('App\Models\FormSelectAnswer');
    }

    /**
     * Get the formMultiSelectAnswers of a Form.
     */
    public function multiSelectAnswers()
    {
        return $this->hasMany('App\Models\FormMultiSelectAnswer');
    }

    /**
     * Get all results of a userForm
     */
    public function getResultAttribute()
    {
        // $form_radio_answer = FormRadioAnswer::where("user_form_id", $this->id)->get()->all();
        // $form_select_answer = FormSelectAnswer::where("user_form_id", $this->id)->get()->all();
        // $form_multi_select_answer = FormMultiSelectAnswer::where("user_form_id", $this->id)->get()->all();

        // // $form_radio_question = FormRadioQuestion::find($form_radio_answer->form_radio_question_id);
        // // $form_select_question = FormSelectQuestion::find($form_select_answer->form_select_question_id);
        // // $form_multi_select_question = FormMultiSelectQuestion::find($form_multi_select_answer->form_multi_select_question_id);


        // // $result = DB::table('user_form')
        // //                 ->join('form_radio_answers', function ($join) {
        // //                     $join->where('form_radio_answers.user_form_id', '=', $this->id);
        // //                 })->get()->all(); 

        // return $result;

        $numberResults = $this->numberAnswers()->with('numberQuestion')->get()->all();
        $textResults = $this->textAnswers()->with('textQuestion')->get()->all();
        $radioResults = $this->radioAnswers()->with('radioQuestion')->get()->all();
        $selectResults = $this->selectAnswers()->with('selectQuestion')->get()->all();
        $multiSelectResults = $this->multiSelectAnswers()->with('multiSelectQuestion')->get()->all();

        foreach ($numberResults as $result) {
            $numbers['question'] = $result->numberQuestion->name;
            $numbers['answer'] = $result->value;
            $array['numbers'][] = $numbers;
        }

        foreach ($textResults as $result) {
            $texts['question'] = $result->textQuestion->name;
            $texts['answer'] = $result->value;
            $array['texts'][] = $texts;
        }

        foreach ($radioResults as $result) {
            $radios['question'] = $result->radioQuestion->name;
            $radios['answer'] = $result->value;
            $array['radios'][] = $radios;
        }

        foreach ($selectResults as $result) {
            $selects['question'] = $result->selectQuestion->name;
            $selects['answer'] = $result->value;
            $array['selects'][] = $selects;
        }

        foreach ($multiSelectResults as $result) {
            $multiSelects['question'] = $result->multiSelectQuestion->name;
            $multiSelects['answer'] = $result->value;
            $array['multiSelects'][] = $multiSelects;
        }

        return $array;
    }
}
