<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAppointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'user_appointment';

    /**
     * Get the visit that is associated to an appointment.
     */
    public function visit()
    {
        return $this->hasOne('App\Models\UserVisit');
    }
}