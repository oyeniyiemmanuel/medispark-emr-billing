<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_service', function (Blueprint $table) {
            $table->id();
            $table->integer('form_id');
            $table->integer('service_id');
            $table->integer('organization_id')->nullable()->comment("references id on gateway.organizations db table");
            $table->integer('branch_id')->nullable()->comment("references id on gateway.branches db table");
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_service');
    }
}
