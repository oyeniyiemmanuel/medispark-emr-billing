<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_form', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id")->nullable()->comment("references id on gateway.users db table");
            $table->integer("form_id")->nullable();
            $table->integer("organization_id")->nullable()->comment("references id on gateway.organizations db table");
            $table->integer("branch_id")->nullable()->comment("references id on gateway.branches db table");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_form');
    }
}
