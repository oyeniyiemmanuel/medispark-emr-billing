<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_user', function (Blueprint $table) {
            $table->id();
            $table->integer("service_id")->nullable()->comment("references id on billing.services db table");
            $table->integer("user_id")->nullable()->comment("references id on gateway.users db table");
            $table->integer("organization_id")->nullable()->comment("references id on gateway.organizations db table");
            $table->integer("branch_id")->nullable()->comment("references id on gateway.branches db table");
            $table->integer("user_visit_id")->nullable()->comment("references id on billing.user_visit db table");
            $table->string("retainer_category")->nullable()->comment("enum of retainer_category");
            $table->string("service_state")->nullable()->comment("enum of service_state");
            $table->string("care_type")->nullable()->comment("enum of care_type");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_user');
    }
}
