<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormTextAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_text_answers', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id")->nullable()->comment("references id on gateway.users db table");
            $table->integer("user_form_id")->nullable();
            $table->string("form_text_question_id")->nullable();
            $table->string("value")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_text_answers');
    }
}
