<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_appointment', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id")->nullable()->comment("references id on gateway.users db table");
            $table->integer("organization_id")->nullable()->comment("references id on gateway.organizations db table");
            $table->integer("branch_id")->nullable()->comment("references id on gateway.branches db table");
            $table->integer("retainer_id")->nullable()->comment("references id on gateway.retainers db table");
            $table->string("status")->nullable()->comment("enum of appointment_status");
            $table->integer("service_id")->nullable()->comment("references id on billing.services db table");
            $table->integer("appointee_id")->nullable()->comment("references id on gateway.users db table");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_appointment');
    }
}
